package com.polygon.game.assets;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;

/*
 * Helper class with all assets used in game with methods to load and unload them.
 * 
 */
public class Assets {
	
	public static Texture splashTexture;
	public static Texture temp;
	public static Skin skin;
	
	public static void loadSplashScreen() {
		boolean mipMaps = true;
        splashTexture = new Texture(Gdx.files.internal("images/logo.png"), mipMaps);
        splashTexture.setFilter(Texture.TextureFilter.MipMapLinearLinear, Texture.TextureFilter.MipMapLinearLinear);
		
	}

	public static void unloadSplashScreen() {
		splashTexture.dispose();
		
	}

	public static void loadMainMenu() {
		skin = new Skin(Gdx.files.internal("UI/Holo.json"));
		temp = new Texture("badlogic.jpg");
		
	}

	public static void UnloadMainMenu() {
		temp.dispose();
		skin.dispose();
	}


}
