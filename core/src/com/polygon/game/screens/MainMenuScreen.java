package com.polygon.game.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.polygon.game.assets.Assets;
import com.polygon.game.screens.minigames.EscapeScreen;
import com.polygon.game.screens.minigames.LockPickScreen;
import com.polygon.game.screens.minigames.PickPocketScreen;

public class MainMenuScreen implements Screen {

	private Stage stage;

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		stage.act(delta);
		stage.draw();

	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub

	}

	@Override
	public void show() {
		Assets.loadMainMenu();
		stage = setStage();
		
	}

	private Stage setStage() {
		//init objects
		Stage stage = new Stage();
		Table table = new Table(Assets.skin);
		Button button1 = new TextButton("Minigame 1", Assets.skin);
		Button button2 = new TextButton("Minigame 2", Assets.skin);
		Button button3 = new TextButton("Minigame 3", Assets.skin);
		Button button4 = new TextButton("Minigame 4", Assets.skin);
		Button button5 = new TextButton("Minigame 5", Assets.skin);
		Button button6 = new TextButton("Minigame 6", Assets.skin);
		
		// add listeners to buttons
		button1.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
			
				((Game) Gdx.app.getApplicationListener()).setScreen(new LockPickScreen());
			}
		});
		button2.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
			
				((Game) Gdx.app.getApplicationListener()).setScreen(new PickPocketScreen());
			}
		});
		button3.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
			
				((Game) Gdx.app.getApplicationListener()).setScreen(new EscapeScreen());
			}
		});
		button4.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
			
				//((Game) Gdx.app.getApplicationListener()).setScreen(new LockPickScreen());
			}
		});
		button5.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
			
				//((Game) Gdx.app.getApplicationListener()).setScreen(new LockPickScreen());
			}
		});
		button6.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
			
				//((Game) Gdx.app.getApplicationListener()).setScreen(new LockPickScreen());
			}
		});
		
		//set up stage
		table.setFillParent(true);
		stage.addActor(table);
		table.add(button1);
		table.add(button2);
		table.add().row();
		table.add(button3);
		table.add(button4);
		table.add().row();
		table.add(button5);
		table.add(button6);
		
		// point input to stage
		Gdx.input.setInputProcessor(stage);
		
		return stage;
	}

	@Override
	public void hide() {
		Assets.UnloadMainMenu();

	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub

	}

}
