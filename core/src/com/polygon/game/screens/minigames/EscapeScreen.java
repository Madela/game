package com.polygon.game.screens.minigames;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.polygon.game.assets.Assets;
import com.polygon.game.screens.MainMenuScreen;

public class EscapeScreen implements Screen {

	private Stage stage;

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		stage.act(delta);
		stage.draw();

	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub

	}

	@Override
	public void show() {
		Assets.loadMainMenu();
		stage = setStage();

	}

	private Stage setStage() {
		// init objects
		Stage stage = new Stage();
		Table table = new Table(Assets.skin);
		Label label = new Label("No Minigame Yet! Escape minigame.", Assets.skin);
		Button back = new TextButton("Back", Assets.skin);

		// setup stage
		table.setFillParent(true);
		stage.addActor(table);
		table.add(label);
		table.add().row();
		table.add(back);

		// add listeners to buttons
		
		back.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {

				((Game) Gdx.app.getApplicationListener()).setScreen(new MainMenuScreen());
			}
		});

		// point input to stage
		Gdx.input.setInputProcessor(stage);

		return stage;
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub

	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub

	}

}
