package com.polygon.game.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

import com.polygon.game.assets.Assets;

public class SplashScreen implements Screen, Runnable {

	private Stage stage;

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		stage.act(delta);
		stage.draw();

	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub

	}

	@Override
	public void show() {
		Assets.loadSplashScreen();
		stage = new Stage();
		
		Image logo = new Image(Assets.splashTexture);
		logo.addAction(Actions.alpha(0.2f));
		logo.addAction(Actions.moveTo((Gdx.graphics.getWidth()-logo.getWidth())/2, (Gdx.graphics.getHeight()-logo.getHeight())/2));
		logo.addAction(Actions.sequence(Actions.alpha(1, 2f), Actions.alpha(0, 2f),Actions.delay(0.5f), Actions.run(this)));
		stage.addActor(logo);
	}

	@Override
	public void hide() {
		stage.dispose();
		Assets.unloadSplashScreen();

	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub

	}

	@Override
	public void run() {
		((Game) Gdx.app.getApplicationListener()).setScreen(new MainMenuScreen());
	}

}
