package com.polygon.game;

import com.badlogic.gdx.Game;
import com.polygon.game.screens.SplashScreen;


public class RpgGame extends Game {

	
	@Override
	public void dispose() {
		super.dispose();
	}

	@Override
	public void render() {
		super.render();
	}

	@Override
	public void resize(int width, int height) {
		super.resize(width, height);
	}

	@Override
	public void pause() {
		super.pause();
	}

	@Override
	public void resume() {
		super.resume();
	}

	@Override
	public void create() {
		this.setScreen(new SplashScreen());
		
	}
}
